
#include <FiniteStateMachine.h>

#include <Button.h>

State On = State(ledOn);
State Off = State(ledOff);
State Blink = State(ledBlink);

FSM ledStateMachine = FSM(Off);
Button b = Button(2, PULLDOWN);
long nPresses;
long previousMillis;
long interval;
int ledState;
long debounce_time;

void setup(){
   pinMode(13, OUTPUT);
   pinMode(12, OUTPUT);
   nPresses = 0;
   previousMillis = 0;
   interval = 500;
   ledState = LOW;
   debounce_time = 200;
}

void loop(){
  
  if(b.uniquePress()){
    nPresses = ++nPresses % 4;
    
    delay(debounce_time);
   
    switch(nPresses){
     case 0: ledStateMachine.transitionTo(Off); break;
     case 1: ledStateMachine.transitionTo(On); break;
     case 2: ledStateMachine.transitionTo(Off); break;
     case 3: ledStateMachine.transitionTo(Blink); break;
    }
  }
  ledStateMachine.update();
}

void ledOn(){
  digitalWrite(13,HIGH);
}

void ledOff(){
  digitalWrite(13, LOW);
}

void ledBlink(){
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis > interval){
    previousMillis = currentMillis;
    if(ledState == HIGH){
      ledState = LOW;
    }else {
      ledState = HIGH;
    }
    digitalWrite(13, ledState);
  }
}
    
   
